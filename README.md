# Query Service JavaScript Technical Exercise

## Task 1
Implement API in NodeJS according specification defined in Swagger spec spec/server-spec.yaml.
There are two critical operations:

* POST /api/v1.1/bds that creates instance asynchronously
* GET /api/v1.1/workRequests returns list of requests to track status, …

### POST /api/v1.1/bds
Validate required input parameters and return client error in case same required parameter/HTTP header is missing.
Log payload in console/file and create work request in memory.

### GET /api/v1.1/workRequest
Returns created work requests from memory.

## Task 2
Implement in React framework single page application based on these UI mockups:
### Create Big Data Instance
![home](img/home.png)

### List Work Requests
![work requests](img/work-requests.png)

## Task 3
Design a system that processes an infinite stream of data:

* each record comes as a tuple(url, html content)
* extract and store the occurrences of: urls, hosts, top-level-domains, in/out links of the page
We have one machine that has enough disk space but limited memory. Write design
document to describe system architecture and data structures as building blocks.

Notes
ETA to complete above tasks is 5 days. Team members will review and comment your sources
and design documents.
